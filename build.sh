#!/bin/bash

# install required
sudo apt-get -y install libusb-dev bluez-tools bluez-hcidump checkinstall libbluetooth-dev joystick pyqt4-dev-tools

# make
cd sixad
make -j8
sudo make install
cd ..

# make sixpair
cd sixpair
./build.sh
cd ..

